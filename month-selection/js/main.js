var d = new Date();

mci.data.month = d.getMonth();

mci.data.year = d.getFullYear();

mci.data.months =  new Array();
	mci.data.months[0] = "JAN";
	mci.data.months[1] = "FEB";
	mci.data.months[2] = "MAR";
	mci.data.months[3] = "APR";
	mci.data.months[4] = "MAY";
	mci.data.months[5] = "JUN";
	mci.data.months[6] = "JUL";
	mci.data.months[7] = "AUG";
	mci.data.months[8] = "SEPT";
	mci.data.months[9] = "OCT";
	mci.data.months[10] = "NOV";
	mci.data.months[11] = "DEC";




for (i = mci.data.year; i > 2003; i--)
{
	mci.data.text = "";
	for (j = 0; j <= 11; j++)
	{
		
		if (i == mci.data.year && j > mci.data.month ) {
			mci.data.text += '<li class="disabled"><span  data-year="' + i + '">'+  mci.data.months[j] +'</span></li>';
		} else {
			mci.data.text += '<li><a  data-year="' + i + '" href="#">'+  mci.data.months[j] +'</a></li>';
		}
}

	$('#calendar').append($('<div class="monthContain"><div class="ui-datepicker-title"><p>' + i + '</p></div><ul>'+mci.data.text+'</ul></div>'));
};


$('#calendar li a').click(function(e){
	e.preventDefault();

	var	$e = $(this),
		year = $e.data('year'),
		month = $e.html(),
		build = year + " " + month,
		buildClass = year + "-" + month
		

	$e.addClass(buildClass);
	
	
	
	if ($e.hasClass('active')) {
		$e.removeClass('active').removeClass(buildClass);
		$('#dateList .'+ buildClass).remove();


	} else {
		$e.addClass('active');
		$('#dateList').append("<li class='"+buildClass+"' data-date='"+buildClass+"'><span class='date'> "+ build + " </span><span class='removeData'>X</span></li>");
		activateRemoveData();
	}
	$('#dateField').empty();
	buildList();
	
});

function buildList(){
	mci.data.value = "";
	var text = $('#dateList li .date').each(function(i,e){
		if($(e).text() !== undefined)
			mci.data.value += $(e).text()+",";
	});

	$('#dateField').val(mci.data.value.replace(/,(\s+)?$/,''));
}

function activateRemoveData(){
	$('.removeData').unbind('click').click(function(e){
		e.preventDefault();
		var $e = $(this),
		buildClass = $e.parent().data("date");
		

		$e.parent().remove();
		$("#calendar").find('a.'+buildClass+'').removeClass('active').removeClass(buildClass);
		buildList();
		
	});
};



$('#clearAllDates').click(function(e){
	e.preventDefault();
	$('#calendar li a').removeClass('active');
	$('#dateField').val("");
	$('#dateList').empty();
})