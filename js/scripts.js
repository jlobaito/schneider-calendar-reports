$(function(){

	//grab json value and parse to 
	tmp_accounts =  JSON.parse($("#jsonAccounts").val())
	tmp_header = tmp_accounts.accountheader
	
	default_str = '<li><p><input type="radio" name="accounts" id="ACCOUNT" value="AID"><label for="ACCOUNT">ACCOUNT</label></p><p class="availDates">DATES</p></li>';
	for(var i in tmp_accounts.accounts){
		
		var tmp_id = tmp_accounts.accounts[i].id;
		var tmp_h1 = tmp_accounts.accounts[i].h1;
		var tmp_account_name = tmp_accounts.accounts[i].h2;		
		var tmp_date = tmp_accounts.accounts[i].h3;	
			
		tmp_str = default_str.replace(/ACCOUNT/g,tmp_account_name)
		tmp_str = tmp_str.replace(/AID/g,tmp_id)
		tmp_str = tmp_str.replace(/DATES/g,tmp_date)
		
		$('#account-listing').prepend(tmp_str)
		
	}

	
	$('#account-listing').find('input').change( function(){
		//split date into array
		tmp_date_range = $(this).parent().next('.availDates').html().split(' - ')
		$('#startDate').val(tmp_date_range[0])
		$('#endDate').val(tmp_date_range[1])
		$('#AID').val($(this).val())
	})
	
	$("input[name='view']").change( function() {
		$('#report_view').val($(this).val())
	})
	
	
	//for the step1 continue button, keep it disabled until the form values are set
	$('#continue').attr('disabled','disabled').css('cursor','pointer')
	
	

});