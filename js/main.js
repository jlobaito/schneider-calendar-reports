var epo = {data:{},app:{}};

var d = new Date();

epo.data.month = d.getMonth();
epo.data.year = d.getFullYear();
epo.data.months =  new Array();
epo.data.months[0] = "JAN";
epo.data.months[1] = "FEB";
epo.data.months[2] = "MAR";
epo.data.months[3] = "APR";
epo.data.months[4] = "MAY";
epo.data.months[5] = "JUN";
epo.data.months[6] = "JUL";
epo.data.months[7] = "AUG";
epo.data.months[8] = "SEPT";
epo.data.months[9] = "OCT";
epo.data.months[10] = "NOV";
epo.data.months[11] = "DEC";
epo.data.contain = $('.contain');




function getAllDays(startDate, endDate) {
    var s = new Date(startDate);
    var e = new Date(endDate);
	var a = [];

    while(s < e) {
        a.push(s);
        s = new Date(s.setDate(
            s.getDate() + 1
        ))
    }

    return a;
};

//rebuild monthly calendar
function buildCalendar(startDateRange, endDateRange) {
	
	/* Build Monthly Calendar Select */
	/*
	startDateRange and endDateRange should come in as mm/dd/yyyy
	need to parse mm/dd/yyyy to a startyear and startmonth
	
	*/
	$('#calendar').empty();
	start_arr = startDateRange.toString().split("/")
	start_year = parseInt(start_arr[2]) -1
	start_month = start_arr[0]-1
	
	
	end_arr = endDateRange.toString().split('/')
	end_year = parseInt(end_arr[2])
	end_month = end_arr[0]-1

	
	for (i = end_year; i > start_year; i--)
	{
		
		epo.data.text = "";
		for (j = 0; j <= 11; j++)
		{

			if (i == end_year && (j > epo.data.month || j > end_month )) {
				epo.data.text += '<li class="disabled"><span data-year="' + i + '">'+  epo.data.months[j] +'</span></li>';
			} else if (i == parseInt(start_year)+1 && j < start_month){
				epo.data.text += '<li class="disabled"><span data-year="' + i + '">'+  epo.data.months[j] +'</span></li>';
			} else {
				epo.data.text += '<li><a  data-year="' + i + '" href="#">'+  epo.data.months[j] +'</a></li>';
			}
		}

		$('#calendar').append($('<div class="monthContain"><div class="ui-datepicker-title"><p>' + i + '</p></div><ul>'+epo.data.text+'</ul></div>'));
	};
	
	/* Clears dates and active class from calendar on button click */

	$('#clearAllDates').click(function(e){
		clearAllDates();	
	})
	
	$('#calendar li a').click(function(e){
		e.preventDefault();

		var	$e = $(this),
		year = $e.data('year'),
		month = $e.html(),
		build = year + " " + month,
		buildClass = year + "-" + month


		$e.addClass(buildClass);
	
		if ($e.hasClass('active')) {
			$e.removeClass('active').removeClass(buildClass);
			$('#dateList .'+ buildClass).remove();
			//remove histdatebeg and histdateend
			remove_from_date_range(month,year,'monthly')

		} else {
			$e.addClass('active');
			$('#dateList').append("<li class='"+buildClass+"' data-date='"+buildClass+"'><span class='date'> "+ build + " </span><span class='removeDate monthly-date-added'>X</span></li>");
			//add to histdatebeg and histdateend
			add_to_date_range(month,year,'monthly')
			activateRemoveData();
		}
		$('#dateField').empty();
		buildList();
		
		
	
	});
	
}


function remove_from_date_range(m,y,theType) {
	if(theType == 'monthly') {
		//remove histdatebeg and histdateend
		var thisDay = new Date(m + ' ' + y)
		var firstDay = $.datepicker.formatDate('mm/dd/yy',new Date(m + ' ' + y))
		var lastDay = $.datepicker.formatDate('mm/dd/yy',new Date(thisDay.getFullYear(), thisDay.getMonth()+1, 0));
		//remove from string
		tmp_str = $('#histdatebeg').val().replace(firstDay + ',', '');
		$('#histdatebeg').val(tmp_str)
		
		tmp_str = $('#histdateend').val().replace(lastDay + ',', '');
		$('#histdateend').val(tmp_str)
		
		
	}
	
}

function add_to_date_range(m,y,theType) {
	if(theType == 'monthly') {
		//remove histdatebeg and histdateend
		var thisDay = new Date(m + ' ' + y)
		var firstDay = $.datepicker.formatDate('mm/dd/yy',new Date(m + ' ' + y))
		var lastDay = $.datepicker.formatDate('mm/dd/yy',new Date(thisDay.getFullYear(), thisDay.getMonth()+1, 0));
		console.log(firstDay + ',' + lastDay); // last day in January
		tmp_str = $('#histdateend').val() + lastDay + ',';
		$('#histdateend').val(tmp_str)
		
		tmp_str = $('#histdatebeg').val() + firstDay + ',';
		$('#histdatebeg').val(tmp_str)
		
	}
}

function buildDailyCalendar(startDateRange, endDateRange) {
		
		start_arr = startDateRange.toString().split("/")
		start_year = parseInt(start_arr[2])
		start_month = start_arr[0]-1
		
		end_arr = endDateRange.toString().split('/')
		end_year = parseInt(end_arr[2])
		end_month = end_arr[0]-1
		
		//need to build array of disabled dates (everythign before the start date and everything after the end date)
		var today = new Date(endDateRange);
		var y = today.getFullYear();
				
		var end = new Date(startDateRange);
		var ey = end.getFullYear();
		begin = new Date('1/1/' + ey)
		
		new_end_date = new Date(end.setDate(
            end.getDate() - 1
        ))
		disabled = getAllDays('1/1/' + ey, new_end_date);		
		disabled_end_dates = getAllDays(endDateRange, '12/31/' + y);
		var new_disabled = disabled.concat(disabled_end_dates);
		
		
		//define calendar object and its initial settings
		
		$('#full-year').multiDatesPicker({
			mode: "normal",
			numberOfMonths: [4,3],
			stepMonths: 12, //allows calendar to paginate an entire year
			defaultDate: '1/1/'+y,
			altField: "#dateField", //field that is populated with selected dates
			separator: ", ",
			addDisabledDates: new_disabled,
			minDate: '1/1/' + ey, //minDate = (start date from JSON - today's date) converted to days
			maxDate: '12/21/'+y //maxDate = (end date from JSON - today's date) converted to days
		});
		
		
		//clear all dates when #clearAllDates button is clicked
		$('#clearAllDates').click(function(){
			$('#full-year').multiDatesPicker('resetDates');
		});
	
}


/*  Grab value of selected view  */

$('.view-toggle').click(function(){
	var	$e = $(this);
	epo.data.val = $e.val();
});

/* Progress to step 2 */

$('#continue').click(function(e){
	//cannot click or submit until form value are populated
	if( $('#startDate').val() == '' || $('#endDate').val() == '' || $('#AID').val() == "" || $('#report_view').val() == "") {
		//something is not filled out
		if (!$("input[name='view']:checked").val()) {
			//set comparision view to red
			$('#comparison_wrapper').css('border','solid 5px red');
		}else{
			$('#comparison_wrapper').css('border','solid 5px #616469');
			
		}
		
		if (!$("input[name='accounts']:checked").val()) {
			//set comparision view to red
			$('#account-selection').css('border','solid 5px red');
		}else{
			$('#account-selection').css('border','solid 5px #616469');
			
		}
		e.preventDefault();
		return;
	}
	e.preventDefault();
	$('.viewSelect').fadeOut();
	
	//grab selected start and end dates
	tmp_start_date = $('#startDate').val()
	tmp_end_date = $('#endDate').val()
	
	//build monthly calendar
	buildCalendar(tmp_start_date, tmp_end_date)
	epo.data.contain.delay(400).fadeIn();
	$('#calendar' + epo.data.val).delay(400).fadeIn();
	
	//build daily calendar
	buildDailyCalendar(tmp_start_date, tmp_end_date);	
	$('.profiles li.active').removeClass('active').next().removeClass('disabled').addClass('active')
});

/* Profiles sidebar */

$('.profiles li').click(function(){
	
	var $e = $(this);
	if ($e.hasClass('disabled')){

	} else {
		$('.viewSelect').delay(400).fadeIn();
		$('#calendar' + epo.data.val).fadeOut();
		epo.data.contain.fadeOut();
		$('.profiles li').removeClass('active');
		$e.addClass('active');
		activateRemoveData();
		clearAllDates();
	}


})

function buildList(){
	epo.data.value = "";
	var text = $('#dateList li .date').each(function(i,e){
		if($(e).text() !== undefined)
			epo.data.value += $(e).text()+",";
	});

	$('#dateField').val(epo.data.value.replace(/,(\s+)?$/,''));
}

function activateRemoveData(){
	$('.removeData').unbind('click').click(function(e){
		e.preventDefault();
		var $e = $(this),
		buildClass = $e.parent().data("date");
		

		$e.parent().remove();
		$("#calendar").find('a.'+buildClass+'').removeClass('active').removeClass(buildClass);
		buildList();
		
	});
};

function clearAllDates(){
	$('#calendar li a').removeClass('active');
	$('#dateField').val("");
	$('#dateList').empty();
}

//remove date when delete button clicked in list
$(document).on('click', '.removeDate', function (e) {
	//this if statement is to handle the monthly calendar
	if($(this).hasClass('monthly-date-added')) {
		buildClass = $(this).parent('li').attr('class')
		tmp = $('#calendar').find('a.' + buildClass)
		$(tmp).removeClass('active').removeClass(buildClass);
		$('#dateList .'+ buildClass).remove();
		//and remove from histdatebeg and histdateend
		tmp_date_arr = buildClass.split('-')
		remove_from_date_range(tmp_date_arr[1],tmp_date_arr[0],'monthly')
		return false;
		e.preventDefault();
	}	
	var dateIndexToRemove = parseInt($(this).attr('id'));
	$('#full-year').multiDatesPicker('removeIndexes', dateIndexToRemove); 
	//and remove from histdatebeg and histdateend
});






